SPECIAL THANKS TO:
NES_MARTIN (twitch.tv/nes_martin): For helping me fix & create this public version based on my ideas and really bad code.

DIGSHAKE (twitch.tv/digshake): The creator of Zelda II Randomizer & a cool dude.

MY FRIENDS & FOLLOWERS (twitch.tv/angrylarry): Who helped me continue to work on this project and offered a lot of support and suggestions.

DOWNLOAD THE RANDOMIZER:
https://bitbucket.org/digshake/z2randomizer/wiki/Home

GET YOURSELF READY TO RACE THIS GAME:
http://www.speedrunslive.com/faq/#getstarted

SRL GAME NAME:
zelda2hacks




*********************************************************************************************************
ANGRYLARRY'S Z2 TRACKER 1.3alf
*********************************************************************************************************

PLEASE READ the notes and disclaimer below to find out the program's optimal uses, limitations, and how you can support future releases.

TL;DR = Click things to track your progress!




*********************************************************************************************************
TABLE OF CONTENTS
*********************************************************************************************************

1. HOW TO USE THE TRACKER
2. GOALS & LIMITATIONS
3. HOW YOU CAN HELP
4. CONTACT INFORMATION
5. CUSTOMIZATION
6. CHANGE LOG




*********************************************************************************************************
HOW TO USE THE TRACKER
*********************************************************************************************************

As its name implies... you will use this tracker to track the items you get and the locations you got them from during the course of your randomizer run.  Since all items appear in the same cave/palace/encounter as the original version of the game^ (for now, see below), just mark the item you pick up next to the spot where you obtained it.

Left clicks and moving the scroll wheel up over an item box will make the items appear in the order in which you would have obtained them (for the first time) in the vanilla version of the game (based on a typical 100%, All Keys speedrun) with the quest/magic items moved to the end of this list.

Right clicks and moving the scroll wheel down will reverse this order.

The order of the items are:
-Magic Container*
-Heart Container*
-Candle
-Hammer
-Glove%
-Raft
-Boots
-Flute
-Cross
-Magic Key
-Trophy@
-Medicine/Water of Life@
-The Kid@

* Duplicate Magic Containers and Heart Containers were removed from this list.  
@ Palace & New Kasuto item lists do not include the quest/magic items as they can never be found there.
% The Glove is also not on the item list for Palaces 3 & 6 since it will never be their item.

SPELLS are tracked with a simple drop box.  Click the list for the town you want and then select the spell you received.

Optional checkboxes have been created if you wish to track where you have been.  They may appear redundant, but you may want to mark an item ahead of time if you narrowed down its location but have not gone back to get it yet, or just mark where you have been and not an item. 

You can also track the palaces where you placed a gem by clicking or using the scroll wheel on the palace image to turn it into a mountain tile.

RESET will remove everything from your tracker and give you a blank slate.  Click the button once, then confirm, and everything will be removed.


^ This tracker works as of Zelda 2 Randomizer Version 2.27 which keeps item locations in the same palaces/caves/towns as the original game.  The physical location of these items and the items themselves are randomized though.  It just means that an item will always be in "Trophy Cave", and every other place that included an item in the normal version of the game.  Future version or flags may change this however.  We will update this tracker when those changes happen.

FOR MORE INFORMATION... It is recommended that you check out the rules and logic for how the game gets randomized at https://bitbucket.org/digshake/z2randomizer/wiki/Options.md




*********************************************************************************************************
GOALS & LIMITATIONS
*********************************************************************************************************

We would like to make this tracker friendly for a newer player who is just learning the game and for those who have been playing it since its launch.  While there is no limitation to the program based on the intent we have for it, there are some other things we wish to add in future updates.  Those features are:

-A beginner mode to help newer players identify caves at a glance.
-An area to take some notes.
-A place to track town/cave/palace location
-Solution for those who do not play on Windows




*********************************************************************************************************
HOW YOU CAN HELP
*********************************************************************************************************

This tracker (and all future public releases of it) is free to use for personal use, on your stream, or to send to a friend.  If you use the tracker on your stream please consider providing a link for it (and my channel or the randomizer itself) on your stream profile.  Also, while not required, if you enjoy this tracker you can send a donation/tip as a way of saying thanks at https://www.twitchalerts.com/donate/angrylarry.  All donations are used to enhance The AngryLarry Experience on my stream via new games & equipment.  Donors will be publically thanked on my profile and in the documentation for future releases!  (Please let me know if you wish to remain anonymous)

If you have any questions, comments, concerns, ideas for future versions, or find any bugs, send them to me!  While it would be nice for this program to do a lot of things please keep in mind it is also created for ease of use.  Somethings would be too complex to implement, and take too much time "doing" for the user.  We would rather help aid them while they play this game (or race) rather than make the tracker a running diary of everything they do within it.

Also note that while I am not totally against it (more like 99% against, 1% undecided)... it is possible to create something that will give a player possible item locations when it comes to required/common items.  For example, using what is already marked off to indicate the current locations where the glove or raft can be.  Again, while possible, we would really like players to determine where things can be on their own without the aid of "instant" hints.  This will help people learn the game, and it will also make for fair and honest races.




*********************************************************************************************************
CONTACT INFORMATION
*********************************************************************************************************

If you need some help, assistance, or would like to provide some suggestion or report a bug, please feel free to whisper/message me through twitch (twitch.tv/angrylarry), or by email (angrylarrygaming@gmail.com).

Thank you for taking the interest in using the tracker and helping out!

-Team AngryLarry
http://www.twitch.tv/angrylarry




*********************************************************************************************************
CUSTOMIZATION
*********************************************************************************************************

I would be willing to customize a tracker to suit the needs of your stream in case you are looking for something with different dimensions or design.  Please keep in mind that I am not taking any custom work for any new features, just to modify the current version to a format that you would want.  While this version, and all future public versions, will be free to use, I would request a reasonable fee for any customized work.

Send me a message if interested.




*********************************************************************************************************
CHANGE LOG
*********************************************************************************************************
WHAT'S NEW in 1.3alf (CURRENT VERSION)

-Removed the Glove from the item pool for palaces 3 & 6.
-Modified the Spell drop-down boxes so you can no longer type inside them (people were clearing/changing the selection because of their split hotkeys).
-Fixed the image for completed palaces back to a mountain tile.
-Made available to the public!

*********************************************************************************************************
WHAT'S NEW in 1.2alf 

-Provided a thin border around the item so it is more apparent where to click/scroll when you want to list an item.
-Cleaned up some dead code.

*********************************************************************************************************
WHAT'S NEW in 1.1alf

-Fixed a bug where resetting the tracker caused the user to have to click the palace icon an extra time to get it to cycle correctly.
-Also fixed a bug that caused the item list to start based on where it last ended prior to resetting, and not the beginning.

*********************************************************************************************************
WHAT'S NEW in 1.0alf

-Improved the visuals of the town spell lists by including the name of the towns for newer players who may not recognize them by the silly little icons we made.

*********************************************************************************************************
WHAT'S NEW in b0.4

-Added single click responses (left & right) as well as the use of the scroll wheel for all items
-Removed the "Magic/Quest" items from the palace and New Kasuto item pools.

*********************************************************************************************************
WHAT'S NEW in b0.3

-Added single click responses (left & right) as well as the use of the scroll wheel for palace items

*********************************************************************************************************
WHAT'S NEW in b0.2

-Added a way to cycle items backwards by double right clicking.
-Removed the Trophy, Medicine, and Kid from the list of items for New Kasuto and all Palaces.
-Removed the Trophy from the list of items for the Eastern Continent and Maze Island.
-Removed the Glove from the list of items in Palaces 3 & 6.
-Adjusted the size of the tracker and added a background image to improve the overall design.

*********************************************************************************************************
WHAT'S NEW in b0.1

-First build, added all items to cycle by clicking the left mouse button for the appropriate locations.
-Added a way to track the spells received per town, with an indicator on what may be required to get them.
-Palaces can be marked as completed by clicking them, thereby displaying a mountain tile like what happens in vanilla.